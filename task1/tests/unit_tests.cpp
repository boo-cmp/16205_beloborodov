#include <task1.h>
#include "gtest/gtest.h"

namespace task1 {

TEST(Task1Tests, IsItAlive) {
  Wow wow;
  EXPECT_TRUE(wow.ItsAlive());  // Success
}

TEST(Task1Tests, IsItDead) {
  Wow wow;
  EXPECT_FALSE(wow.ItsAlive());  // Fail :(
}

}  // namespace task1
